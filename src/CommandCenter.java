import java.util.ArrayList;

// Вы проектируете интеллектуальную систему управления ангаром командного центра.
// Реализуйте интерфейс SpaceshipFleetManager для управления флотом кораблей.
// Используйте СУЩЕСТВУЮЩИЙ интерфейс и класс космического корабля (SpaceshipFleetManager и Spaceship).
public class CommandCenter implements SpaceshipFleetManager{

      public Spaceship getMostPowerfulShip(ArrayList<Spaceship> ships){
            Spaceship ship = new Spaceship("", 0, 0, 0);
            for (int i = 1; i < ships.size(); i++) {
                  if(ships.get(i).getFirePower()>ship.getFirePower()){
                        ship = ships.get(i);
                  }
            }
            if(ship.getFirePower()==0){
                  return null;
            }
            return ship;
      };

      public Spaceship getShipByName(ArrayList<Spaceship> ships, String name){

            for (int i = 0; i < ships.size(); i++) {
                  if(ships.get(i).getName().equals(name)){
                        return ships.get(i);
                  }
            }
            return null;
      };

      public ArrayList<Spaceship> getAllShipsWithEnoughCargoSpace(ArrayList<Spaceship> ships, Integer cargoSize){
            ArrayList<Spaceship> needShips = new ArrayList<>();

            for (int i = 0; i < ships.size(); i++) {
                  if(ships.get(i).getCargoSpace()>=cargoSize){
                        needShips.add(ships.get(i));
                  }
            }
            return needShips;
      };

      public ArrayList<Spaceship> getAllCivilianShips(ArrayList<Spaceship> ships){
            ArrayList<Spaceship> shipsWithoutGuns = new ArrayList<>();

            for (int i = 0; i < ships.size(); i++) {
                  if(ships.get(i).getFirePower()==0){
                        shipsWithoutGuns.add(ships.get(i));
                  };
            }

            return shipsWithoutGuns;
      };

}