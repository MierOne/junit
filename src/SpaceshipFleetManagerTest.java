import java.util.*;

public class SpaceshipFleetManagerTest {
      SpaceshipFleetManager center = new CommandCenter();

      ArrayList<Spaceship> testList = new ArrayList<Spaceship>();

      public static void main(String[] args) {
            SpaceshipFleetManagerTest spaceshipFleetManagerTest = new SpaceshipFleetManagerTest();

            int points = 0;

            if(spaceshipFleetManagerTest.getMostPowerfulShip_returnMostPowerfulShip() && spaceshipFleetManagerTest.getMostPowerfulShip_returnNull()){
                  points++;
            }

            if(spaceshipFleetManagerTest.getShipByName_returnShipByName("Meta") && spaceshipFleetManagerTest.getShipByName_returnNull("no")){
                  points++;
            }

            if(spaceshipFleetManagerTest.getAllCivilianShips_returnCivilianSpaceshipsList() && spaceshipFleetManagerTest.getAllCivilianShips_returnEmptyList()){
                  points++;
            }

            if(spaceshipFleetManagerTest.getAllShipsWithEnoughCargoSpace_returnSpaceshipsListWithEnoughCargoSpace(20) && spaceshipFleetManagerTest.getAllShipsWithEnoughCargoSpace_returnEmptyList(1000)){
                  points++;
            }

            System.out.println(points + " теста из 4 были успешны. Набрано " + points + " балл(а)");
      }

      private boolean getMostPowerfulShip_returnMostPowerfulShip() {
            testList.clear();

            Spaceship spaceship1 = new Spaceship("Lesnoy", 0, 0, 20000);
            Spaceship spaceship2 = new Spaceship("Pudge", 42, 0, 0);
            Spaceship spaceship3 = new Spaceship("Is", 12, 54, 0);
            Spaceship spaceship4 = new Spaceship("Meta", 6, 23, 24);
            Spaceship spaceship5 = new Spaceship("Play", 8, 2, 6);
            Spaceship spaceship6 = new Spaceship("Manчик", 0, 0, 0);

            testList.add(spaceship1);
            testList.add(spaceship2);
            testList.add(spaceship3);
            testList.add(spaceship4);
            testList.add(spaceship5);
            testList.add(spaceship6);

            Spaceship mostPfShip = center.getMostPowerfulShip(testList);

            for (int i = 0; i < testList.size(); i++) {
                  if (testList.get(i).getFirePower()>mostPfShip.getFirePower()) {
                        return false;
                  }
            }

            return true;
      }

      private boolean getMostPowerfulShip_returnNull() {
            testList.clear();

            Spaceship spaceship1 = new Spaceship("Lesnoy", 0, 0, 20000);
            Spaceship spaceship2 = new Spaceship("Pudge", 0, 0, 0);
            Spaceship spaceship3 = new Spaceship("Is", 0, 54, 0);
            Spaceship spaceship4 = new Spaceship("Meta", 0, 23, 24);
            Spaceship spaceship5 = new Spaceship("Play", 0, 2, 6);
            Spaceship spaceship6 = new Spaceship("Manчик", 0, 0, 0);

            testList.add(spaceship1);
            testList.add(spaceship2);
            testList.add(spaceship3);
            testList.add(spaceship4);
            testList.add(spaceship5);
            testList.add(spaceship6);

            Spaceship mostPfShip = center.getMostPowerfulShip(testList);

            if (mostPfShip == null) {
                  return true;
            }

            return false;
      }

      private boolean getShipByName_returnShipByName(String name) {
            testList.clear();

            Spaceship spaceship1 = new Spaceship("Lesnoy", 0, 0, 20000);
            Spaceship spaceship2 = new Spaceship("Pudge", 42, 0, 0);
            Spaceship spaceship3 = new Spaceship("Is", 12, 54, 0);
            Spaceship spaceship4 = new Spaceship("Meta", 6, 23, 24);
            Spaceship spaceship5 = new Spaceship("Play", 8, 2, 6);
            Spaceship spaceship6 = new Spaceship("Manчик", 0, 0, 0);

            testList.add(spaceship1);
            testList.add(spaceship2);
            testList.add(spaceship3);
            testList.add(spaceship4);
            testList.add(spaceship5);
            testList.add(spaceship6);

            Spaceship ship = center.getShipByName(testList, name);

            if(ship.getName().equals(name)){
                  return true;
            }

            return false;
      }

      private boolean getShipByName_returnNull(String name) {
            testList.clear();

            Spaceship spaceship1 = new Spaceship("Lesnoy", 0, 0, 20000);
            Spaceship spaceship2 = new Spaceship("Pudge", 42, 0, 0);
            Spaceship spaceship3 = new Spaceship("Is", 12, 54, 0);
            Spaceship spaceship4 = new Spaceship("Meta", 6, 23, 24);
            Spaceship spaceship5 = new Spaceship("Play", 8, 2, 6);
            Spaceship spaceship6 = new Spaceship("Manчик", 0, 0, 0);

            testList.add(spaceship1);
            testList.add(spaceship2);
            testList.add(spaceship3);
            testList.add(spaceship4);
            testList.add(spaceship5);
            testList.add(spaceship6);

            Spaceship ship = center.getShipByName(testList, name);

            if(ship==null){
                  return true;
            }

            return false;
      }

      private boolean getAllShipsWithEnoughCargoSpace_returnSpaceshipsListWithEnoughCargoSpace(int cargoSpace) {
            testList.clear();

            Spaceship spaceship1 = new Spaceship("Lesnoy", 0, 0, 20000);
            Spaceship spaceship2 = new Spaceship("Pudge", 42, 0, 0);
            Spaceship spaceship3 = new Spaceship("Is", 12, 54, 0);
            Spaceship spaceship4 = new Spaceship("Meta", 6, 23, 24);
            Spaceship spaceship5 = new Spaceship("Play", 8, 2, 6);
            Spaceship spaceship6 = new Spaceship("Manчик", 0, 0, 0);

            testList.add(spaceship1);
            testList.add(spaceship2);
            testList.add(spaceship3);
            testList.add(spaceship4);
            testList.add(spaceship5);
            testList.add(spaceship6);

            ArrayList<Spaceship> testList1 = center.getAllShipsWithEnoughCargoSpace(testList, cargoSpace);

            for (int i = 0; i < testList1.size(); i++) {
                  if(testList1.get(i).getCargoSpace()<cargoSpace){
                        return false;
                  }
            }
            return true;
      }

      private boolean getAllShipsWithEnoughCargoSpace_returnEmptyList(int cargoSpace) {
            testList.clear();

            Spaceship spaceship1 = new Spaceship("Lesnoy", 0, 0, 20000);
            Spaceship spaceship2 = new Spaceship("Pudge", 42, 0, 0);
            Spaceship spaceship3 = new Spaceship("Is", 12, 54, 0);
            Spaceship spaceship4 = new Spaceship("Meta", 6, 23, 24);
            Spaceship spaceship5 = new Spaceship("Play", 8, 2, 6);
            Spaceship spaceship6 = new Spaceship("Manчик", 0, 0, 0);

            testList.add(spaceship1);
            testList.add(spaceship2);
            testList.add(spaceship3);
            testList.add(spaceship4);
            testList.add(spaceship5);
            testList.add(spaceship6);

            ArrayList<Spaceship> testList1 = center.getAllShipsWithEnoughCargoSpace(testList,cargoSpace);

            if (testList1.size() == 0) {
                  return true;
            }

            return false;
      }

      private boolean getAllCivilianShips_returnCivilianSpaceshipsList() {
            testList.clear();

            Spaceship spaceship1 = new Spaceship("Lesnoy", 0, 0, 20000);
            Spaceship spaceship2 = new Spaceship("Pudge", 42, 0, 0);
            Spaceship spaceship3 = new Spaceship("Is", 12, 54, 0);
            Spaceship spaceship4 = new Spaceship("Meta", 6, 23, 24);
            Spaceship spaceship5 = new Spaceship("Play", 8, 2, 6);
            Spaceship spaceship6 = new Spaceship("Manчик", 0, 0, 0);

            testList.add(spaceship1);
            testList.add(spaceship2);
            testList.add(spaceship3);
            testList.add(spaceship4);
            testList.add(spaceship5);
            testList.add(spaceship6);

            ArrayList<Spaceship> testList1 = center.getAllCivilianShips(testList);

            for (int i = 0; i < testList1.size(); i++) {
                  if(testList1.get(i).getFirePower()!=0){
                        return false;
                  }
            }
            return true;
      }

      private boolean getAllCivilianShips_returnEmptyList() {
            testList.clear();

            Spaceship spaceship1 = new Spaceship("Lesnoy", 4, 0, 20000);
            Spaceship spaceship2 = new Spaceship("Pudge", 42, 0, 0);
            Spaceship spaceship3 = new Spaceship("Is", 12, 54, 0);
            Spaceship spaceship4 = new Spaceship("Meta", 6, 23, 24);
            Spaceship spaceship5 = new Spaceship("Play", 8, 2, 6);
            Spaceship spaceship6 = new Spaceship("Manчик", 68, 0, 0);

            testList.add(spaceship1);
            testList.add(spaceship2);
            testList.add(spaceship3);
            testList.add(spaceship4);
            testList.add(spaceship5);
            testList.add(spaceship6);

            ArrayList<Spaceship> testList1 = center.getAllCivilianShips(testList);

            if(testList1.size()==0){
                  return true;
            }

            return false;
      }

}