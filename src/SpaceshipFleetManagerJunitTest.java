import org.junit.jupiter.api.*;

import java.util.*;

public class SpaceshipFleetManagerJunitTest {
      SpaceshipFleetManager center = new CommandCenter();

      ArrayList<Spaceship> testList = new ArrayList<>();

      @BeforeEach
      void clearTestList() {
            testList.clear();
      }

      @AfterEach

      @Test
      void getShipByName_returnShipByName() {
            Spaceship spaceship1 = new Spaceship("Lesnoy", 0, 0, 20000);
            Spaceship spaceship2 = new Spaceship("Pudge", 42, 0, 0);
            Spaceship spaceship3 = new Spaceship("Is", 12, 54, 0);
            Spaceship spaceship4 = new Spaceship("Meta", 6, 23, 24);
            Spaceship spaceship5 = new Spaceship("Play", 8, 2, 6);
            Spaceship spaceship6 = new Spaceship("Manчик", 0, 0, 0);

            testList.add(spaceship1);
            testList.add(spaceship2);
            testList.add(spaceship3);
            testList.add(spaceship4);
            testList.add(spaceship5);
            testList.add(spaceship6);

            String name = "Pudge";

            Spaceship ship = center.getShipByName(testList, name);

            Assertions.assertEquals(name, ship.getName());
      }

      @Test
      void getShipByName_returnNull() {
            Spaceship spaceship1 = new Spaceship("Lesnoy", 0, 0, 20000);
            Spaceship spaceship2 = new Spaceship("Pudge", 42, 0, 0);
            Spaceship spaceship3 = new Spaceship("Is", 12, 54, 0);
            Spaceship spaceship4 = new Spaceship("Meta", 6, 23, 24);
            Spaceship spaceship5 = new Spaceship("Play", 8, 2, 6);
            Spaceship spaceship6 = new Spaceship("Manчик", 0, 0, 0);

            testList.add(spaceship1);
            testList.add(spaceship2);
            testList.add(spaceship3);
            testList.add(spaceship4);
            testList.add(spaceship5);
            testList.add(spaceship6);

            String name = "LEEEENNYYYYYYY";

            Spaceship ship= center.getShipByName(testList, name);

            Assertions.assertNull(ship);
      }

      @Test
      void getMostPowerfulShip_returnMostPowerfulShip() {
            Spaceship spaceship1 = new Spaceship("Lesnoy", 0, 0, 2000);
            Spaceship spaceship2 = new Spaceship("Pudge", 42, 0, 0);
            Spaceship spaceship3 = new Spaceship("Is", 12, 54, 0);
            Spaceship spaceship4 = new Spaceship("Meta", 6, 23, 24);
            Spaceship spaceship5 = new Spaceship("Play", 8, 2, 6);
            Spaceship spaceship6 = new Spaceship("Manчик", 0, 0, 0);

            testList.add(spaceship1);
            testList.add(spaceship2);
            testList.add(spaceship3);
            testList.add(spaceship4);
            testList.add(spaceship5);
            testList.add(spaceship6);

            Spaceship mostPfShip = center.getMostPowerfulShip(testList);

            Assertions.assertEquals(42, mostPfShip.getFirePower());
      }

      @Test
      void getMostPowerfulShip_returnNull() {
            Spaceship spaceship1 = new Spaceship("Lesnoy", 0, 0, 20000);
            Spaceship spaceship2 = new Spaceship("Pudge", 0, 0, 0);
            Spaceship spaceship3 = new Spaceship("Is", 0, 54, 0);
            Spaceship spaceship4 = new Spaceship("Meta", 0, 23, 24);
            Spaceship spaceship5 = new Spaceship("Play", 0, 2, 6);
            Spaceship spaceship6 = new Spaceship("Manчик", 0, 0, 0);

            testList.add(spaceship1);
            testList.add(spaceship2);
            testList.add(spaceship3);
            testList.add(spaceship4);
            testList.add(spaceship5);
            testList.add(spaceship6);

            Spaceship mostPfShip = center.getMostPowerfulShip(testList);

            Assertions.assertNull(mostPfShip);
      }

      @Test
      void getAllShipsWithEnoughCargoSpace_returnSpaceshipsListWithEnoughCargoSpace() {
            Spaceship spaceship1 = new Spaceship("Lesnoy", 0, 0, 20000);
            Spaceship spaceship2 = new Spaceship("Pudge", 42, 0, 0);
            Spaceship spaceship3 = new Spaceship("Is", 12, 54, 0);
            Spaceship spaceship4 = new Spaceship("Meta", 6, 23, 24);
            Spaceship spaceship5 = new Spaceship("Play", 8, 2, 6);
            Spaceship spaceship6 = new Spaceship("Manчик", 0, 0, 0);

            testList.add(spaceship1);
            testList.add(spaceship2);
            testList.add(spaceship3);
            testList.add(spaceship4);
            testList.add(spaceship5);
            testList.add(spaceship6);

            int cargoSpace = 20;

            ArrayList<Spaceship> output = center.getAllShipsWithEnoughCargoSpace(testList, cargoSpace);

            for (int i = 0; i < output.size() ; i++) {
                  Assertions.assertTrue(output.get(i).getCargoSpace() > cargoSpace);
            }
      }

      @Test
      void getAllShipsWithEnoughCargoSpace_returnEmptyList() {
            Spaceship spaceship1 = new Spaceship("Lesnoy", 0, 0, 20000);
            Spaceship spaceship2 = new Spaceship("Pudge", 42, 0, 0);
            Spaceship spaceship3 = new Spaceship("Is", 12, 54, 0);
            Spaceship spaceship4 = new Spaceship("Meta", 6, 23, 24);
            Spaceship spaceship5 = new Spaceship("Play", 8, 2, 6);
            Spaceship spaceship6 = new Spaceship("Manчик", 0, 0, 0);

            testList.add(spaceship1);
            testList.add(spaceship2);
            testList.add(spaceship3);
            testList.add(spaceship4);
            testList.add(spaceship5);
            testList.add(spaceship6);

            int cargoSapce = 2000;

            ArrayList<Spaceship> output = center.getAllShipsWithEnoughCargoSpace(testList, cargoSapce);

            Assertions.assertEquals(output.size(), 0);
      }

      @Test
      void getAllCivilianShips_returnCivilianSpaceshipsList() {
            Spaceship spaceship1 = new Spaceship("Lesnoy", 0, 0, 20000);
            Spaceship spaceship2 = new Spaceship("Pudge", 42, 0, 0);
            Spaceship spaceship3 = new Spaceship("Is", 12, 54, 0);
            Spaceship spaceship4 = new Spaceship("Meta", 6, 23, 24);
            Spaceship spaceship5 = new Spaceship("Play", 8, 2, 6);
            Spaceship spaceship6 = new Spaceship("Manчик", 0, 0, 0);

            testList.add(spaceship1);
            testList.add(spaceship2);
            testList.add(spaceship3);
            testList.add(spaceship4);
            testList.add(spaceship5);
            testList.add(spaceship6);

            ArrayList<Spaceship> output = center.getAllCivilianShips(testList);

            for (int i = 0; i < output.size() ; i++) {
                  Assertions.assertEquals(output.get(i).getFirePower(), 0);
            }
      }

      @Test
      void getAllCivilianShips_returnEmptyList() {
            Spaceship spaceship1 = new Spaceship("Lesnoy", 4, 0, 20000);
            Spaceship spaceship2 = new Spaceship("Pudge", 42, 0, 0);
            Spaceship spaceship3 = new Spaceship("Is", 12, 54, 0);
            Spaceship spaceship4 = new Spaceship("Meta", 6, 23, 24);
            Spaceship spaceship5 = new Spaceship("Play", 8, 2, 6);
            Spaceship spaceship6 = new Spaceship("Manчик", 32, 0, 0);

            testList.add(spaceship1);
            testList.add(spaceship2);
            testList.add(spaceship3);
            testList.add(spaceship4);
            testList.add(spaceship5);
            testList.add(spaceship6);

            ArrayList<Spaceship> output = center.getAllCivilianShips(testList);

            for (int i = 0; i < output.size() ; i++) {
                  Assertions.assertEquals(output.size(), 0);
            }
      }
}
